# OOP meets FP und warum ich Java trotzdem mag

Beispiel Code zu meinem Vortrag beim Java Forum Nord 2019 in Hannover.

* [Java Forum Nord Webseite](https://javaforumnord.de/site/2019/) 
* [Folien zum Vortrag](https://docs.google.com/presentation/d/14vFXcQ3CqetFqm24qc7BpB5JjGw7G6i4ZdIAwR-pYMM/edit?usp=sharing)

## Abstract

Java ist eine objektorientierte Programmiersprache. 
Sie ist nun über 20 Jahre halt und ist im TIOBE Index nie unter Platz 2 gefallen.
Mit Version 8 sind neue Features wie Functional Interfaces, Lambda Expressions, Method References, Optional und die Stream API hinzugekommen.

In diesem Vortrag möchte ich die funktionale Programmierung mit Java in den Fokus stellen. 
Welche neuen Features der einzelnen Java Updates unterstützen die funktionale Programmierung und wie kann man dieses Paradigma im Programmier-Alltag nutzen. 
Welche Eigenschaften und Konzepte fehlen bei Java, um rein funktional zu programmieren.
Mit Code-Beispielen möchte ich zeigen, wie gut (oder auch schlecht) Java für die funktionale Programmierung geeignet ist.

## Referenzen

* [OO vs FP von Robert C. Martin](http://blog.cleancoder.com/uncle-bob/2014/11/24/FPvsOO.html)
* [Functional programming for Java developers von Jeff Friesen - Teil 1](https://www.javaworld.com/article/3314640/functional-programming-for-java-developers-part-1.html)
* [Functional programming for Java developers von Jeff Friesen - Teil 2](https://www.javaworld.com/article/3319078/functional-programming-for-java-developers-part-2.html)
* [From Gof to Lambda von Mario Fusco](https://github.com/mariofusco/from-gof-to-lambda)
* [vavr Dokumentation](https://www.vavr.io/vavr-docs/#_introduction)