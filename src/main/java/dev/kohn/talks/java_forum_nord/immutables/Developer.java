package dev.kohn.talks.java_forum_nord.immutables;

import java.util.List;
import org.immutables.value.Value;

@Value.Immutable
public abstract class Developer {

  public abstract String name();
  public abstract List<String> programmingLanguages();
}
