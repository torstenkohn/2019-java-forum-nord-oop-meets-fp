package dev.kohn.talks.java_forum_nord.immutables;

public class ImmutablesExampleUsage {

  public static void main(String... args) {
    Developer dev = ImmutableDeveloper.builder()
        .name("Torsten")
        .addProgrammingLanguages("Java", "JavaScript", "TypeScript")
        .build();

    dev.name(); // Torsten
    dev.programmingLanguages(); // [Java, JavaScript, TypeScript]
  }
}
