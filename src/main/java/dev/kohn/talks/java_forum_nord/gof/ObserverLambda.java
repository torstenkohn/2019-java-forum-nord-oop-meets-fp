package dev.kohn.talks.java_forum_nord.gof;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

/**
 * Source code from Mario Fusco
 * See also: https://github.com/mariofusco/from-gof-to-lambda
 */
public class ObserverLambda {

  static class Observable {

    private final Map<String, Consumer<String>> listeners = new HashMap<>();

    void register(String key, Consumer<String> listener) {
      listeners.put(key, listener);
    }

    void unregister(String key) {
      listeners.remove(key);
    }

    void sendEvent(String event) {
      listeners.values().forEach(listener -> listener.accept(event));
    }
  }

  public static void main(String[] args) {
    Observable observable = new Observable();
    observable.register("key1", System.out::println);
    observable.register("key2", System.out::println);

    observable.sendEvent("Hello World!");
    observable.unregister("key2");
    observable.sendEvent("Hello Java Forum Nord!");
  }
}