package dev.kohn.talks.java_forum_nord.gof;

import java.util.HashMap;
import java.util.Map;

/**
 * Source code from Mario Fusco
 * See also: https://github.com/mariofusco/from-gof-to-lambda
 */
public class Observer {

  interface Listener {
    void onEvent(Object event);
  }

  public static class Observable {
    private final Map<Object, Listener> listeners = new HashMap<>();

    public void register(Object key, Listener listener) {
      listeners.put(key, listener);
    }

    public void unregister(Object key) {
      listeners.remove(key);
    }

    public void sendEvent(Object event) {
      for (Listener listener : listeners.values()) {
        listener.onEvent( event );
      }
    }
  }

  public static class Observer1 {
    Observer1(Observable observable) {
      observable.register( this, new Listener() {
        @Override
        public void onEvent( Object event ) {
          System.out.println(event);
        }
      } );
    }
  }

  public static class Observer2 implements Listener {
    Observer2(Observable observable) {
      observable.register( this, this );
    }
    @Override
    public void onEvent( Object event ) {
      System.out.println(event);
    }
  }

  public static void main( String[] args ) {
    Observable observable = new Observable();
    new Observer1( observable );
    Observer2 observer2 = new Observer2(observable);

    observable.sendEvent("Hello World!");
    observable.unregister(observer2);
    observable.sendEvent("Hello Java Forum Nord!");
  }
}