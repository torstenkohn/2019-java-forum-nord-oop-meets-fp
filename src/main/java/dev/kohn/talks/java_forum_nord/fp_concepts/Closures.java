package dev.kohn.talks.java_forum_nord.fp_concepts;

import java.util.function.Function;

public class Closures {

  public static void main(String[] args) {
    Function<Integer, Integer> add25 = add(25);
    System.out.println(add25.apply(5)); // 30
    System.out.println(add25.apply(-25)); // 0
  }

  private static Function<Integer, Integer> add(int number) {
    return n -> number + n;
  }

}
