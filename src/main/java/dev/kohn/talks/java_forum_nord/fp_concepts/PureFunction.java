package dev.kohn.talks.java_forum_nord.fp_concepts;

import java.util.function.Function;

public class PureFunction {

  public static void main(String[] args) {

    Function<Integer, Integer> square = n -> n * n;
    System.out.println(square(5));
    System.out.println(square.apply(5));
  }

  private static int square(int number){
    return number * number;
  }

}
