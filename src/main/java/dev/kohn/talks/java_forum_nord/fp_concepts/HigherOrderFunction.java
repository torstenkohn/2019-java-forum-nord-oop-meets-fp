package dev.kohn.talks.java_forum_nord.fp_concepts;

import java.util.Comparator;
import java.util.stream.IntStream;

public class HigherOrderFunction {

  public static void main(String[] args) {

    Comparator<Integer> integerComparator = Comparator.naturalOrder();

    IntStream
        .of(7, 100, 89, -5, 13, 4000)
        .boxed()
        .sorted(integerComparator)
        .forEach(System.out::println);
  }

}
