package dev.kohn.talks.java_forum_nord.fp_concepts;

import java.util.Optional;
import java.util.stream.IntStream;

public class LazyEvaluation {

  public static void main(String[] args) {
    Optional<Integer> result = IntStream.rangeClosed(1, Integer.MAX_VALUE)
        .boxed()
        .parallel()
        //.peek(n -> System.out.println("1: " + n))
        .filter(n -> n % 45 == 0)
        //.peek(n -> System.out.println("2: " + n))
        .filter(n -> n % 2 == 0)
        //.peek(n -> System.out.println("3: " + n))
        .map(n -> n / 5)
        //.peek(n -> System.out.println("4: " + n))
        .findFirst();

    result.ifPresent(System.out::println);

  }

}
