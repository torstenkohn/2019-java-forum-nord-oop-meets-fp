package dev.kohn.talks.java_forum_nord.fp_concepts;

/**
 * Stackoverflow anser from missingfactor see also:  https://stackoverflow.com/a/6134321/5591387
 *
 * Does Java support currying?
 */

interface Function1<A, B> {

  B apply(final A a);
}

interface Function2<A, B, C> {

  C apply(final A a, final B b);
}

public class CurryingJava7 {

  private static Function2<Integer, Integer, Integer> simpleAdd =
      new Function2<Integer, Integer, Integer>() {
        public Integer apply(final Integer a, final Integer b) {
          return a + b;
        }
      };

  private static Function1<Integer, Function1<Integer, Integer>> curriedAdd =
      new Function1<Integer, Function1<Integer, Integer>>() {
        public Function1<Integer, Integer> apply(final Integer a) {
          return new Function1<Integer, Integer>() {
            public Integer apply(final Integer b) {
              return a + b;
            }
          };
        }
      };

  public static void main(String[] args) {
    // Demonstrating simple `add`
    System.out.println("simpleAdd: 4 + 5 -> " + simpleAdd.apply(4, 5));

    // Demonstrating curried `add`
    System.out.println("curriedAdd 4 + 5 -> " + curriedAdd.apply(4).apply(5));

    // Curried version lets you perform partial application
    // as demonstrated below.
    Function1<Integer, Integer> adder5 = curriedAdd.apply(5);
    System.out.println(adder5.apply(4));
    System.out.println(adder5.apply(6));
  }

}


