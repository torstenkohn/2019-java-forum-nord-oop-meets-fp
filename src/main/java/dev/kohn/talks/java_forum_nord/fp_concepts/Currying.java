package dev.kohn.talks.java_forum_nord.fp_concepts;

import java.util.function.Function;
import java.util.function.IntBinaryOperator;
import java.util.function.IntFunction;
import java.util.function.IntUnaryOperator;

public class Currying {

  public static void main(String[] args) {
    IntBinaryOperator multiply = (a, b) -> a * b;
    System.out.println(
        multiply.applyAsInt(3, 5)
    );

    IntFunction<IntUnaryOperator> multiplyCurried = a -> b -> a * b;
    System.out.println(
        multiplyCurried.apply(5).applyAsInt(3)
    );





    Function<Integer,
        Function<Integer,
            Function<Integer,
                Function<Integer, Integer>>>> calc =
        a -> b -> c -> d -> (a + b) * (c + d);

    Function<Integer, Function<Integer, Function<Integer, Integer>>> calc1 = calc.apply(1);
    Function<Integer, Function<Integer, Integer>> calc2 = calc1.apply(2);
    Function<Integer, Integer> calc3 = calc2.apply(3);
    Integer result = calc3.apply(4);
    System.out.println(result);
  }

}
