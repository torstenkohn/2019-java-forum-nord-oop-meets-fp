package dev.kohn.talks.java_forum_nord.vavr;

import static io.vavr.API.$;
import static io.vavr.API.Case;
import static io.vavr.API.Match;

import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class MatchExample {

  public static void main(String[] args) {
    Predicate<Integer> isFizz = n  -> n % 3 == 0;
    Predicate<Integer> isBuzz = n  -> n % 5 == 0;
    Predicate<Integer> isFizzBuzz = n  -> n % 15 == 0;

    Function<Integer, String> fizzBuzz = n -> Match(n).of(
        Case($(isFizzBuzz), "FizzBuzz"),
        Case($(isFizz), "Fizz"),
        Case($(isBuzz), "Buzz"),
        Case($(), String.valueOf(n))
    );

    List<String> result = IntStream
        .rangeClosed(1, 30)
        .boxed()
        .map(fizzBuzz)
        .collect(Collectors.toList());
    // [1, 2, Fizz, 4, Buzz, Fizz, 7, 8, Fizz, Buzz, 11, Fizz, 13, 14, FizzBuzz, 16, ...]

    result.forEach(System.out::println);
  }

}
