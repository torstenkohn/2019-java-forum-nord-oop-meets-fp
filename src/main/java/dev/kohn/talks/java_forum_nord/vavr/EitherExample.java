package dev.kohn.talks.java_forum_nord.vavr;

import io.vavr.control.Either;
import java.util.function.Function;
import java.util.stream.IntStream;

public class EitherExample {

  public static void main(String[] args) {
    Function<Integer, Either<Integer, String>> divideByTwo = num -> {
      if (num % 2 == 0) {
        return Either.left(num / 2);
      }
      return Either.right(num + " is not even, can not handle it");
    };

    IntStream
        .rangeClosed(-10, 10)
        .sequential()
        .boxed()
        .map(divideByTwo)
        .filter(Either::isLeft)
        .map(Either::getLeft)
        .forEach(System.out::println);
  }
}
