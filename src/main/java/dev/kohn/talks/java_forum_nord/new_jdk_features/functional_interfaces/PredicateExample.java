package dev.kohn.talks.java_forum_nord.new_jdk_features.functional_interfaces;

import java.util.function.Predicate;
import java.util.stream.Stream;

public class PredicateExample {

  public static void main(String[] args) {
    Predicate<Integer> isPositiveNumber = a -> a >= 0;
    System.out.println("          isPositiveNumber.test(-1) -> " +
        isPositiveNumber.test(-1));

    Predicate<Integer> isZero = a -> a == 0;
    System.out.println("isZero.or(isPositiveNumber).test(7) -> " +
        isZero.or(isPositiveNumber).test(7));

    Stream
        .of(-5, 4, 3, -100, 0, 0, 0, 1422, -23)
        .filter(isPositiveNumber)
        .forEach(System.out::println);
  }

}
