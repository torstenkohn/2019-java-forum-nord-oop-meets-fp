package dev.kohn.talks.java_forum_nord.new_jdk_features.functional_interfaces;

import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ConsumerExample {

  public static void main(String[] args) {
    Consumer<String> print = System.out::println;

    Consumer<String> printWithPrefix = s -> System.out.println("PREFIX-" + s);

    List<String> result = IntStream
        .rangeClosed(1, 10)
        .boxed()
        .map(String::valueOf)
        .collect(Collectors.toList());

    result.forEach(n -> print.andThen(printWithPrefix).accept(n));
  }
}
