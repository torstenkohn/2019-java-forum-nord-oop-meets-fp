package dev.kohn.talks.java_forum_nord.new_jdk_features.lambda_expression;

import java.util.function.Function;

public class LambdaExpressionExample {

  public static void main(String[] args) {
    new Thread(() -> System.out.println("Hallo Java Forum Nord!"))
        .start(); // Hallo Java Forum Nord!

    Function<Integer, Integer> square = a -> a * a;
    System.out.println(square.apply(5)); // 25
  }

}
