package dev.kohn.talks.java_forum_nord.new_jdk_features.method_reference;

import java.util.Optional;

public class MethodReferenceExample {

  public static void main(String[] args) {
    HelloService service = new HelloService();

    String result = Optional
        .of("Java Forum Nord")
        .map(Conference::new) // Reference to a constructor
        .map(Conference::getName) // Reference to an instance method of an arbitrary object of a particular type
        .map(service::sayHello) // Reference to an instance method of a particular object
        .map(MethodReferenceExample::replaceOwithZero) // Reference to a static method
        .orElse("No Conference!");

    System.out.println("Result: " + result); // Result: Hell0 Java F0rum N0rd!
  }

  private static String replaceOwithZero(String source) {
    return source.replace("o", "0");
  }
}

class Conference {

  private final String name;

  Conference(String name) {
    this.name = name;
  }

  String getName() {
    return name;
  }
}

class HelloService {

  String sayHello(String to) {
    return String.format("Hello %s!", to);
  }
}
