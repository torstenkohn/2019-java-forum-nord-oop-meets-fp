package dev.kohn.talks.java_forum_nord.new_jdk_features.stream_api;

import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class StreamApiExample {

  public static void main(String[] args) {

    Predicate<Integer> isEven = n -> n % 2 == 0;
    Function<Integer, Integer> square = a -> a * a;

    List<Integer> firstList = IntStream.rangeClosed(-5, 5)
        .boxed()
        .filter(isEven) // [-4, -2, 0, 2, 4]
        .map(square) // [16, 4, 0, 4, 16]
        .collect(Collectors.toList()); // [16, 4, 0, 4, 16]

    Integer sum = firstList.stream().reduce(0, Integer::sum);
    System.out.println(sum); // 40
  }

}
