package dev.kohn.talks.java_forum_nord.new_jdk_features.optional;

import java.util.Optional;

public class OptionalExample {

  public static void main(String[] args) {
    String hello1 = Optional
        .of("Hello Java Forum Nord!")
        .map(s -> s.toLowerCase())
        .get();
    System.out.println(hello1); // hello java forum nord!

    Optional<String> hello2 = Optional
        .<String>ofNullable(null)
        .map(s -> s.toUpperCase());

    System.out.println(hello2.isEmpty()); // true
    System.out.println(hello2.isPresent()); // false
    System.out.println(hello2.orElse("Hello World!")); // Hello World!

  }

}
