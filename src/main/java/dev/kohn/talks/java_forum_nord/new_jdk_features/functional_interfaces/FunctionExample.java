package dev.kohn.talks.java_forum_nord.new_jdk_features.functional_interfaces;

import java.util.function.Function;

public class FunctionExample {

  public static void main(String[] args) {

    Function<Integer,
        Function<Integer,
            Function<Integer,
                Function<Integer, Integer>>>> calc =
        a -> b -> c -> d -> (a + b) * (c + d);

    System.out.println("(1 + 2) * (3 + 4) = " +
        calc.apply(1).apply(2).apply(3).apply(4));
  }

}
